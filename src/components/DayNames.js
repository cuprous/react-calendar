import React, { Component } from "react";
import moment from "moment";
import "../resources/css/DayNames.css";

/** Represents the names of the days of the week that appears nears the top of a calendar. */
class DayNames extends Component {
  /**
   * @returns {array} Returns an array of paragraph elements containing Sunday to Saturday.
   */
  dayNames() {
    const days = [];
    const day = moment()
      .startOf("day")
      .day("Sunday");

    for (let i = 0; i < 7; i++) {
      days.push(
        <p key={i}>
          {day
            .clone()
            .add(i, "day")
            .format("dddd")}
        </p>
      );
    }
    return days;
  }

  render() {
    return <div className="DayNames">{this.dayNames()}</div>;
  }
}

export { DayNames };
