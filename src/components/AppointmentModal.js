import React, { Component } from "react";
import { Modal, Button } from "react-bootstrap";
import { AppointmentForm } from "./AppointmentForm";

class AppointmentModal extends Component {
  render() {
    return (
      <Modal
        show={this.props.show}
        onHide={this.props.handleClose}
        bsSize="small"
        backdrop="static"
      >
        <Modal.Header closeButton>
          <Modal.Title>Create an Appointment</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AppointmentForm
            startTime={this.props.startTime}
            endTime={this.props.endTime}
            appointmentTitle={this.props.appointmentTitle}
            handleTitleChange={this.props.handleTitleChange}
            handleStartTimeChange={this.props.handleStartTimeChange}
            handleEndTimeChange={this.props.handleEndTimeChange}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.handleClose}>Close</Button>
          <Button bsStyle="primary" onClick={this.props.handleSave}>
            Save
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export { AppointmentModal };
