import React, { Component } from "react";
import { AppointmentModal } from "./AppointmentModal";
import moment from "moment";
import swal from "sweetalert2";
import "../resources/css/AppointmentButton.css";

class AppointmentButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false
    };
  }

  handleClose = () => {
    this.setState({
      appointmentTitle: "",
      showModal: false
    });
  };

  handleShow = () => {
    this.setState({
      showModal: true,
      appointmentTitle: "",
      endTime: moment().add(1, "hour"),
      startTime: moment()
    });
  };

  handleTitleChange = event => {
    this.setState({ appointmentTitle: event.target.value });
  };

  handleStartTimeChange = startTime => {
    this.setState({ startTime }, () => {
      const endTime = this.state.startTime.clone().add(1, "hour");
      this.handleEndTimeChange(endTime);
    });
  };

  handleEndTimeChange = endTime => {
    this.setState({ endTime });
  };

  handleSave = () => {
    const start = this.state.startTime;
    const end = this.state.endTime;
    const title = this.state.appointmentTitle;

    this.props
      .createAppointment(start, end, title)
      .then(appointment => {
        this.props.pushAppointment(appointment);
        this.handleClose();
      })
      .catch(error => {
        if (error.title && error.message) {
          swal(error.title, error.message, "error");
        } else {
          swal("Oops!", error, "error");
        }
      });
  };

  render() {
    return (
      <div>
        <div className="AppointmentButton" onClick={this.handleShow}>
          <i className="fa fa-plus fa-fw" />
        </div>
        <AppointmentModal
          show={this.state.showModal}
          appointmentTitle={this.state.appointmentTitle}
          endTime={this.state.endTime}
          startTime={this.state.startTime}
          handleClose={this.handleClose}
          handleTitleChange={this.handleTitleChange}
          handleStartTimeChange={this.handleStartTimeChange}
          handleEndTimeChange={this.handleEndTimeChange}
          handleSave={this.handleSave}
        />
      </div>
    );
  }
}

export { AppointmentButton };
