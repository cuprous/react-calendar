import React from "react";
import App from "./App";
import { render, shallow } from "enzyme";
import { Appointment } from "./Appointment";
import moment from "moment";

function getAppointment() {
  const start = moment("2018-01-01 00:00:00");
  const end = start.clone().add(1, "hour");
  return (
    <Appointment
      key={start.unix()}
      title={"Mock Title"}
      start={start}
      end={end}
    />
  );
}

describe("<App />", () => {
  it("renders without crashing", () => {
    render(<App />);
  });
});

let wrapper;

beforeEach(() => {
  wrapper = shallow(<App />);
});

describe("App.prototype.pushAppointment(appointment)", () => {
  it("adds appointments to the appointments state array", () => {
    const appointmentsStartingLength = wrapper.instance().state.appointments
      .length;
    wrapper.instance().pushAppointment(getAppointment());
    const appointmentsEndingLength = wrapper.instance().state.appointments
      .length;
    expect(appointmentsEndingLength - appointmentsStartingLength).toBe(1);
  });
});

describe("App.prototype.deleteAppointment(appointment)", () => {
  it("removes appoints from the appointments state array", () => {
    wrapper.instance().setState({ appointments: [getAppointment()] });
    const appointmentsStartingLength = wrapper.instance().state.appointments
      .length;
    wrapper.instance().deleteAppointment(getAppointment());
    const appointmentsEndingLength = wrapper.instance().state.appointments
      .length;
    expect(appointmentsStartingLength - appointmentsEndingLength).toBe(1);
  });
});
