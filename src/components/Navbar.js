import React, { Component } from "react";
import "../resources/css/Navbar.css";

class Navbar extends Component {
  render() {
    return (
      <div className="Nav">
        <div className="nav-buttons">
          <div
            className="nav-button"
            role="button"
            onClick={this.props.decrementMonth}
          >
            <i className="fa fa-angle-left fa-lg fa-fw" />
          </div>
          <div
            className="nav-button"
            role="button"
            onClick={this.props.incrementMonth}
          >
            <i className="fa fa-angle-right fa-lg fa-fw" />
          </div>
        </div>
        <div className="nav-month">{this.props.month.format("MMMM Y")}</div>
      </div>
    );
  }
}

export { Navbar };
