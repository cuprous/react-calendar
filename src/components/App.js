import { Appointment } from "./Appointment";
import { AppointmentButton } from "./AppointmentButton";
import { Calendar } from "./Calendar";
import React, { Component } from "react";
import moment from "moment";
import "../resources/css/App.css";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      appointments: [
        <Appointment
          key={moment("2018-02-14 16:30:00").unix()}
          deleteAppointment={this.deleteAppointment}
          className="Appointment"
          start={moment("2018-02-14 16:30:00")}
          end={moment("2018-02-10 17:15:00")}
          title={"Bike Ride"}
        />,
        <Appointment
          key={moment("2018-02-11 08:30:00").unix()}
          deleteAppointment={this.deleteAppointment}
          className="Appointment"
          start={moment("2018-02-11 08:30:00")}
          end={moment("2018-02-11 18:00:00")}
          title={"Ikea Trip"}
        />,
        <Appointment
          key={moment("2018-02-20 19:00:00").unix()}
          deleteAppointment={this.deleteAppointment}
          className="Appointment"
          start={moment("2018-02-20 19:00:00")}
          end={moment("2018-02-20 21:00:00")}
          title={"Concert"}
        />
      ]
    };
  }

  createAppointment = (start, end, title) => {
    return new Promise((resolve, reject) => {
      // Reject empty titles.
      if (title.length === 0) {
        reject({
          title: "Missing Title",
          message: "You must provide a title for the appointment."
        });
      }
      // Reject events that start in the past
      if (moment().isAfter(start, "day") || moment().isAfter(start, "hour")) {
        reject({
          title: "You're Late",
          message: "Your appointment cannot start in the past."
        });
      }
      // Reject events that end before they start.
      if (end.isBefore(start)) {
        reject({
          title: "Invalid Times",
          message: "Your appointment cannot end before it begins."
        });
      }
      // Reject overlapping events.
      const overlappingIndex = this.overlappingIndex(start, end);
      if (overlappingIndex !== -1) {
        const appointment = this.state.appointments[overlappingIndex];
        const start = appointment.props.start;
        const end = appointment.props.end;
        const title = appointment.props.title;

        reject({
          title: "Conflicting Appointment",
          message: `The appointment you are trying to schedule conflicts with ${title} that is scheduled for ${start.format(
            "MMM D [at] h:mm A"
          )} to ${end.format("MMM D [at] h:mm A")} and cannot be created.`
        });
      }

      resolve(
        <Appointment
          key={start.unix()}
          start={start}
          end={end}
          title={title}
          className="Appointment"
          deleteAppointment={this.deleteAppointment}
        />
      );
    });
  };

  deleteAppointment = appointment => {
    const appointments = this.state.appointments.slice();
    const index = appointments.findIndex(element => {
      return (
        element.props.title === appointment.props.title &&
        element.props.start.diff(appointment.props.start) === 0 &&
        element.props.end.diff(appointment.props.end) === 0
      );
    });
    appointments.splice(index, 1);
    this.setState({ appointments });
  };

  pushAppointment = appointment => {
    const appointments = this.state.appointments.slice();
    appointments.push(appointment);
    this.setState({ appointments });
  };

  // Look into an IntervalTree for this later.
  overlappingIndex = (start, end) => {
    return this.state.appointments.findIndex(appointment => {
      return (
        Math.max(start.unix(), appointment.props.start.unix()) <
        Math.min(end.unix(), appointment.props.end.unix())
      );
    });
  };

  render() {
    return (
      <div className="App">
        <Calendar appointments={this.state.appointments} />
        <AppointmentButton
          createAppointment={this.createAppointment}
          pushAppointment={this.pushAppointment}
        />
      </div>
    );
  }
}

export default App;
