import React from "react";
import { Appointment } from "./Appointment";
import { render } from "enzyme";
import moment from "moment";

const start = moment("2018-01-01 00:00:00");
const end = start.clone().add(1, "hour");

describe("<Appointment />", () => {
  it("renders without crashing", () => {
    render(<Appointment title="Mock title" start={start} end={end} />);
  });
});
