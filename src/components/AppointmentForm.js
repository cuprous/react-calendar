import React, { Component } from "react";
import { Form, FormGroup, ControlLabel, FormControl } from "react-bootstrap";
import DateTime from "react-datetime";
import moment from "moment";

class AppointmentForm extends Component {
  isValidStart = day => {
    return this.isValid(day, moment().subtract(1, "day"));
  };

  isValidEnd = day => {
    return this.isValid(day, this.props.startTime.clone().subtract(1, "day"));
  };

  isValid = (day, target) => {
    return day.isAfter(target);
  };

  startTimeConstraints = () => {
    return this.timeConstraints(moment().add(5, "minute"));
  };

  endTimeConstraints = () => {
    return this.timeConstraints(this.props.startTime.clone().add(5, "minute"));
  };

  render() {
    return (
      <Form>
        <FormGroup controlId="formTitle">
          <ControlLabel>Add Title</ControlLabel>
          <FormControl
            type="text"
            value={this.props.appointmentTitle}
            onChange={this.props.handleTitleChange}
          />
        </FormGroup>
        <FormGroup controlId="formStartTime">
          <ControlLabel>Start Time</ControlLabel>
          <DateTime
            inputProps={{ readOnly: true }}
            isValidDate={this.isValidStart}
            value={this.props.startTime}
            onChange={this.props.handleStartTimeChange}
          />
        </FormGroup>
        <FormGroup controlId="formEndTime">
          <ControlLabel>End Time</ControlLabel>
          <DateTime
            inputProps={{ readOnly: true }}
            isValidDate={this.isValidEnd}
            value={this.props.endTime}
            onChange={this.props.handleEndTimeChange}
          />
        </FormGroup>
      </Form>
    );
  }
}

export { AppointmentForm };
