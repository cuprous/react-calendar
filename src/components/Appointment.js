import React, { Component } from "react";
import swal from "sweetalert2";
import '../resources/css/Appointment.css';

class Appointment extends Component {
  appointmentInfo = () => {
    const config = {
      title: `${this.props.title}`,
      text: this.appointmentText(),
      showCancelButton: true,
      focusConfirm: false,
      cancelButtonColor: "#dc3545",
      confirmButtonText: "Close",
      cancelButtonText: "Delete"
    };

    swal(config).then(event => {
      if (event.dismiss === "cancel") {
        this.props.deleteAppointment(this);
      }
    });
  };

  appointmentText = () => {
    return `${this.props.start.format("h:mm A")} to ${this.props.end.format(
      "h:mm A"
    )}`;
  };

  render() {
    return (
      <div className="Appointment" onClick={this.appointmentInfo}>
        <p>{this.appointmentText()}</p>
      </div>
    );
  }
}

export { Appointment };
