import React, { Component } from "react";
import moment from "moment";
import "../resources/css/Day.css";

/** Represents a calendar day. */
class Day extends Component {
  /**
   * @returns {boolean} Returns true if the day is found within the current month prop.
   */
  inCurrentMonth = () => {
    const monthStart = this.props.month.clone().startOf("month");
    const monthEnd = this.props.month.clone().endOf("month");

    return (
      !this.props.day.isBefore(monthStart) && !this.props.day.isAfter(monthEnd)
    );
  };

  /**
   * @returns {array} Returns a sorted array of Appointments occurring on this day.
   */
  appointments = () => {
    return this.props.appointments
      .filter(appointment => {
        return (
          this.props.day.format("DDD") === appointment.props.start.format("DDD") &&
          this.props.day.format("Y") === appointment.props.start.format("Y")
        );
      })
      .sort((a, b) => {
        return a.key - b.key;
      });
  };

  /**
   * @returns {boolean} Returns true if this.props.day is today, i.e., right now.
   */
  isToday = () => {
    return (
      this.props.day.format("DDD") === moment().format("DDD") &&
      this.props.day.format("Y") === moment().format("Y")
    );
  };

  /**
   * @returns {string} Returns a string of all class names application to this day.
   */
  classNames = () => {
    let classNames = "";

    if (!this.inCurrentMonth()) {
      classNames += "outOfMonth ";
    }

    if (this.isToday()) {
      classNames += "today ";
    }

    return classNames;
  };

  render() {
    return (
      <div className="Day">
        <span className={this.classNames()}>{this.props.day.format("D")}</span>
        {this.appointments()}
      </div>
    );
  }
}

export { Day };
