import React, { Component } from "react";
import moment from "moment";
import { DayNames } from "./DayNames";
import { Day } from "./Day";
import { Navbar } from "./Navbar";
import "../resources/css/Calendar.css";

class Calendar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      month: moment()
    };
  }

  incrementMonth = () => {
    const month = this.state.month.clone();
    month.add(1, "M");
    this.setState({ month });
  };

  decrementMonth = () => {
    const month = this.state.month.clone();
    month.subtract(1, "M");
    this.setState({ month });
  };

  days() {
    const days = [];
    const daysPerMonth = 42;
    const startOfCalendar = this.state.month
      .clone()
      .startOf("month")
      .day("Sunday");

    for (let i = 0; i < daysPerMonth; i++) {
      days.push(
        <Day
          key={i}
          day={startOfCalendar.clone().add(i, "day")}
          month={this.state.month}
          appointments={this.props.appointments}
        />
      );
    }

    return days;
  }

  render() {
    return (
      <div className="Calendar">
        <Navbar
          month={this.state.month}
          incrementMonth={this.incrementMonth}
          decrementMonth={this.decrementMonth}
        />
        <DayNames />
        {this.days()}
      </div>
    );
  }
}

export { Calendar };
